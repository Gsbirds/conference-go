DEBUG = True
ALLOWED_HOSTS = []
LANGUAGE_CODE = "en-us"
TIME_ZONE = "UTC"
USE_I18N = True
USE_TZ = True

# EMAIL_HOST = "mail"
# EMAIL_SUBJECT_PREFIX = ""
# EMAIL_PORT =
# EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"
EMAIL_HOST = "host.docker.internal"
EMAIL_PORT = 1025
