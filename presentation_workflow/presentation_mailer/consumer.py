import json
import pika
from pika.exceptions import AMQPConnectionError
import time
import django
import os
import sys
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()

while True:
    try:

        def process_approval(ch, method, properties, body):
            print("  Received %r" % body)
            presentation = json.loads(body)
            name = presentation["presenter_name"]
            email = presentation["presenter_email"]
            title = presentation["title"]
            send_mail(
                "accepted",
                f"{name}, your presentation {title} is accepted",
                "admin@conference.go",
                ["something@localhost"],
                fail_silently=False,
            )
            print("sent email")

        def process_rejection(ch, method, properties, body):
            print("  Received %r" % body)
            presentation = json.loads(body)
            name = presentation["presenter_name"]
            email = presentation["presenter_email"]
            title = presentation["title"]
            send_mail(
                "rejected",
                f"{name}, your presentation {title} is rejected",
                "admin@conference.go",
                [f"{email}"],
                fail_silently=False,
            )

        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        print("1")

        channel.queue_declare(queue="presentation_approvals")
        channel.basic_consume(
            queue="presentation_approvals",
            on_message_callback=process_approval,
            auto_ack=True,
        ),
        channel.queue_declare(queue="presentation_rejections")
        channel.basic_consume(
            queue="presentation_rejections",
            on_message_callback=process_rejection,
            auto_ack=True,
        )
        print("2")
        channel.start_consuming()
        print("3")
    except AMQPConnectionError as e:
        print(f"Could not connect to RabbitMQ err: {e}")
        time.sleep(2.0)
